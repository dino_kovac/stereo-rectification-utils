#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

int main(int argc, char **argv) {

    if (argc != 3) {
        std::cerr << "Usage: " << argv[0] << " <image path> <calib params yml>" << std::endl;
        return -1;
    }

    std::string imageFile = argv[1];
    std::string calib_params_file = argv[2];

    cv::FileStorage inputParams(calib_params_file, CV_STORAGE_READ);
    cv::Mat cameraMatrix, distCoeffs;
    inputParams["camera_matrix"] >> cameraMatrix;
    inputParams["distortion_coefficients"] >> distCoeffs;
    inputParams.release();
    std::cout << cameraMatrix << std::endl << std::endl;

    cv::Mat image = cv::imread(imageFile, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat image2 = cv::imread(imageFile, CV_LOAD_IMAGE_GRAYSCALE);

    if (!image.data) {
        std::cerr << "Error loading image!" << std::endl;
        return -1;
    }

    cv::undistort(image, image2, cameraMatrix, distCoeffs);

    cv::imwrite("undistorted.png", image2);

    return 0;
}