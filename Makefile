INC=`pkg-config --cflags opencv`
OPTIONS=`pkg-config --libs opencv`
CXXFLAGS=-c -O3 -std=c++11 -Wall -Wextra
LDFLAGS=-O3

all: undistort rectify

.cc.o:
	$(CXX) $(CXXFLAGS) $< -o $@ $(INC)

undistort: undistort.cc
	$(CXX) $(LDFLAGS) undistort.cc -o $@ $(INC) $(OPTIONS)

rectify: rectify.cc
	$(CXX) $(LDFLAGS) rectify.cc -o $@ $(INC) $(OPTIONS)

clean:
	-rm undistort rectify

.PHONY: clean
