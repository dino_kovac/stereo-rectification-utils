#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

void stereoLocal(cv::Mat leftImage, cv::Mat rightImage);

void stereoSemiGlobal(cv::Mat leftImage, cv::Mat rightImage);

int main(int argc, char **argv) {
    
    if (argc == 3) {
        std::string leftImageFile = argv[1];
        std::string rightImageFile = argv[2];

        cv::Mat leftImage = cv::imread(leftImageFile, CV_LOAD_IMAGE_GRAYSCALE);
        cv::Mat rightImage = cv::imread(rightImageFile, CV_LOAD_IMAGE_GRAYSCALE);

        if (!leftImage.data || !rightImage.data) {
            std::cerr << "Error loading one of the images! Aborting." << std::endl;
            return -1;
        }

        if (leftImage.size() != rightImage.size()) {
            std::cerr << "The images must be equal in size! Aborting." << std::endl;
            return -1;
        }

        stereoLocal(leftImage, rightImage);
        stereoSemiGlobal(leftImage, rightImage);
    } else if (argc == 2) {
        std::string disparityMapFile = argv[1];

        cv::Mat disparityMap = cv::imread(disparityMapFile, CV_LOAD_IMAGE_GRAYSCALE);

        if (!disparityMap.data) {
            std::cerr << "Error loading the disparity map! Aborting." << std::endl;
            return -1;
        }

        cv::normalize(disparityMap, disparityMap, 0, 255, CV_MINMAX, CV_16S);
        cv::imwrite("normalized-disparity-map.png", disparityMap);
    } else {
        std::cerr << "Usage: " << argv[0] << " <disparity map path>" << std::endl;
        std::cerr << "Usage: " << argv[0] << " <left image path> <right image path>" << std::endl;
        return -1;
    }

    return 0;
}

void stereoLocal(cv::Mat leftImage, cv::Mat rightImage) {
    cv::StereoBM sbm;
    sbm.state->SADWindowSize = 9;
    sbm.state->numberOfDisparities = 64;
    sbm.state->preFilterSize = 5;
    sbm.state->preFilterCap = 61;
    sbm.state->minDisparity = 0;
    sbm.state->textureThreshold = 507;
    sbm.state->uniquenessRatio = 0;
    sbm.state->speckleWindowSize = 0;
    sbm.state->speckleRange = 8;
    sbm.state->disp12MaxDiff = 1;

    cv::Mat disparityMap;
    sbm(leftImage, rightImage, disparityMap, CV_16S);
    cv::normalize(disparityMap, disparityMap, 0, 255, CV_MINMAX, CV_16S);

    cv::imwrite("stereobm.png", disparityMap);
}

void stereoSemiGlobal(cv::Mat leftImage, cv::Mat rightImage) {
    cv::StereoSGBM sgbm;
    sgbm.SADWindowSize = 9;
    sgbm.numberOfDisparities = 64;
    sgbm.preFilterCap = 4;
    sgbm.minDisparity = 0;
    sgbm.uniquenessRatio = 1;
    sgbm.speckleWindowSize = 150;
    sgbm.speckleRange = 2;
    sgbm.disp12MaxDiff = 10;
    sgbm.fullDP = false;
    sgbm.P1 = 600;
    sgbm.P2 = 2400;

    cv::Mat disparityMap;
    sgbm(leftImage, rightImage, disparityMap);
    cv::normalize(disparityMap, disparityMap, 0, 255, CV_MINMAX, CV_16S);

    cv::imwrite("stereosgbm.png", disparityMap);
}
