#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

void rectifyImage(cv::Mat &image, cv::Mat &cameraMatrix, cv::Mat &distCoeffs, cv::Mat &R, cv::Mat &P,
                  cv::Mat &outImage) {
    cv::Mat rectMap[2];
    // Precompute maps for cv::remap()
    cv::initUndistortRectifyMap(cameraMatrix, distCoeffs, R, P, image.size(), CV_16SC2, rectMap[0], rectMap[1]);
    remap(image, outImage, rectMap[0], rectMap[1], CV_INTER_LINEAR);
}

void saveRectifiedImage(cv::Mat &image, std::string filename) {
    unsigned long lastDotIndex = filename.find_last_of('.');
    if (lastDotIndex == std::string::npos) {
        std::cerr << "Dot not found in filename! Aborting." << std::endl;
        exit(-2);
    }
    std::string name = filename.substr(0, lastDotIndex) + "_rectified" + filename.substr(lastDotIndex);
    cv::Mat colorImage;
    cvtColor(image, colorImage, CV_GRAY2BGR);
    cv::imwrite(name, colorImage);
}

void showResult(cv::Mat &leftImage, cv::Mat &rightImage) {
    int rows = leftImage.rows;
    int cols = leftImage.cols;
    cv::Mat canvas(rows, 2 * cols, CV_8UC3);
    cv::Mat canvasLeft = canvas(cv::Rect(0, 0, cols, rows));
    cv::Mat canvasRight = canvas(cv::Rect(cols, 0, cols, rows));

    cv::cvtColor(leftImage, canvasLeft, CV_GRAY2BGR);
    cv::cvtColor(rightImage, canvasRight, CV_GRAY2BGR);

    cv::resize(canvas, canvas, cv::Size(canvas.cols / 2, canvas.rows / 2), 0, 0, CV_INTER_AREA);

    for (int j = 0; j < canvas.rows; j += 16) {
        cv::line(canvas, cv::Point(0, j), cv::Point(canvas.cols, j), cv::Scalar(0, 255, 0), 1, 8);
    }

    cv::imshow("result", canvas);
    cv::waitKey(0);
}

int main(int argc, char **argv) {

    if (argc != 4) {
        std::cerr << "Usage: " << argv[0] << " <left image path> <right image path> <calibration params yml>" <<
        std::endl;
        return -1;
    }

    std::string leftImageFile = argv[1];
    std::string rightImageFile = argv[2];
    std::string calibrationParamsFile = argv[3];

    cv::Mat leftImage = cv::imread(leftImageFile, CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat rightImage = cv::imread(rightImageFile, CV_LOAD_IMAGE_GRAYSCALE);

    if (!leftImage.data || !rightImage.data) {
        std::cerr << "Error loading one of the images! Aborting." << std::endl;
        return -1;
    }

    if (leftImage.size() != rightImage.size()) {
        std::cerr << "The images must be equal in size! Aborting." << std::endl;
        return -1;
    }

    cv::FileStorage calibrationParams(calibrationParamsFile, CV_STORAGE_READ);
    cv::Mat leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs;
    cv::Mat R, T, E, F;
    calibrationParams["M1"] >> leftCameraMatrix;
    calibrationParams["D1"] >> leftDistCoeffs;
    calibrationParams["M2"] >> rightCameraMatrix;
    calibrationParams["D2"] >> rightDistCoeffs;
    calibrationParams["R"] >> R;
    calibrationParams["T"] >> T;
    calibrationParams.release();

    cv::Mat rectLeftImage, rectRightImage;

    cv::Mat R1, R2, P1, P2, Q;
    cv::Size newImageSize;
    cv::Rect validRoi[2];

    double alpha = -1; // 0 to zoom to ROI, 1 not to crop, -1 default crop
    cv::stereoRectify(leftCameraMatrix, leftDistCoeffs, rightCameraMatrix, rightDistCoeffs,
                      leftImage.size(), R, T, R1, R2, P1, P2, Q, CV_CALIB_ZERO_DISPARITY, alpha, newImageSize,
                      &validRoi[0], &validRoi[1]);

    rectifyImage(leftImage, leftCameraMatrix, leftDistCoeffs, R1, P1, rectLeftImage);
    rectifyImage(rightImage, rightCameraMatrix, rightDistCoeffs, R2, P2, rectRightImage);

    std::cout << R1 << std::endl << R2 << std::endl;

    cv::Mat leftRodrigues, rightRodrigues;

    cv::Rodrigues(R1, leftRodrigues);
    cv::Rodrigues(R2, rightRodrigues);

    std::cout << leftRodrigues << std::endl << rightRodrigues << std::endl;

    saveRectifiedImage(rectLeftImage, leftImageFile);
    saveRectifiedImage(rectRightImage, rightImageFile);

    showResult(rectLeftImage, rectRightImage);
    return 0;
}